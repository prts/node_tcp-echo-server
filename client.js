/* 
Connect from the command line to echo server 'server.js'
$ node client.js

You can pass an string argument to cutomize the sending message
$ node client.js AA
 */

var _TEXT = (process.argv[2] || '') + ' Client sending Hello Echo server!'
var _EXIT = false;

var _PORT = 50000
var _HOST = '127.0.0.1'

var net = require('net');

var client = new net.Socket();

client.connect(_PORT, _HOST, function() {
  //console.log('Connected to ',client.address());
  console.log('Connected to ', client.remoteAddress + ':' + client.remotePort);
});

client.on('data', function(data) {
  console.log('Received: ' + data);
  if (data.toString().slice(0,22)!='Welcome to Echo server' && data.toString()!=_TEXT) {
    console.log('Bad Received: ' + _TEXT);
    process.exit(1);
  }
  if (!_EXIT) {
    setTimeout(function() {
      console.log('Sending : ' + _TEXT);
      client.write(_TEXT);
    },1000);
    //console.log('Sending : ' + _TEXT)
    //client.write(_TEXT);
  } else client.end();
});

client.on('end', function() {
  //process.stdout.write('client disconnected ... ');
  console.log('Client ended.');
});

client.on('close', function() {
  console.log('Client closed.');
});

client.on('error', function(err) {
  console.log('Client error:', err.message);
});

process.on('SIGINT', function() {
  _EXIT = true;
});
