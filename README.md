node_tcp-echo-server
=


Another Node tcp echo server ...   
Thanks to all folks writing and sharing similar code.

### Server
Start the tcp echo server
```bash
$ node server.js
```

### Client
Connect from the command line with the tcp client 'client.js' or use netcat.
```bash
$ node client.js
$ netcat 127.0.0.1 50000
```


You can pass a simple string argument to customize the sending message.
```bash
$ node client.js MyString
```

