/*
Start the tcp echo server
$ node server.js

Connect from the command line with the tcp client 'client.js' or netcat.
$ node client.js
$ netcat 127.0.0.1 50000

*/

var _PORT = 50000
var _HOST = '0.0.0.0'

var net = require('net');
var clients = [];

var server = net.createServer();

server.on('connection', function(socket) {
  var myRA = socket.remoteAddress;
  console.log('New connection from ' + myRA);
  clients.push(socket);
  socket.write('Welcome to Echo server ' + myRA);
  
  socket.on('data', function(data) {
    console.log('got data ' + data);
    socket.write(data);
  });
  
  socket.on('end', function() {
    //process.stdout.write('client disconnected ... ');
    console.log(myRA + ' client disconnected.');
  });
  
  socket.on('error', function(err) {
    console.log(myRA + ' error:');
    console.log(err.message);
  });
  
  socket.on('close', function(has_err) {
    msg = myRA;
    clients.splice(clients.indexOf(socket), 1);
    if (has_err) msg += ' Closed with errors.'; else msg += ' Closed.';
    console.log(msg);
  });
  
});

server.on('listening', function() {
  console.log('Echo Server listening:', server.address());
});

server.on('error', function(err) {
  console.log('Server error:', err.message);
});
server.on('close', function() {
  console.log('Server closed.');
});

server.listen(_PORT, _HOST);

process.on('SIGINT', function() {
  server.close();
  //for (var i in clients) clients[i].destroy();
  for (var i in clients) clients[i].end();
});
